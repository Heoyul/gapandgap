﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class SelectLesson : MonoBehaviour
{
    public Button lesson1;
    public Action LessonChanged;

    public void Lesson1Clicked()
    {
        LessonChanged?.Invoke();
        Debug.Log("Lesson1");
    }
}
