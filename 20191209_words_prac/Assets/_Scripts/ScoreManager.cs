﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;

    public int score =0;

    public Action<int> ScoreChanged;

    void Start()
    {
        ScoreChanged?.Invoke(score);
    }

    public void AddScore()
    {
        score += 10;
        ScoreChanged?.Invoke(score);
        Debug.Log(score);
     }

    void Awake()
    {
        ScoreManager.instance = this;
    }



}
