﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class FeedBack : MonoBehaviour
{
    public static FeedBack instance;
  
    public static int FinalScore = 0;
    public static int LevelText = 0;
    public Text ShowedFinalText, ShowedLevelText;

    // Start is called before the first frame update
    void Start()
    {
        ShowedFinalText = GetComponent<Text>();
        ShowedLevelText = GetComponent<Text>();
        Final();

    }


    public void Final()
    {
        FinalScore = ScoreManager.instance.score;
        LevelText = GameManager.currentLevel;
        Debug.Log(FinalScore);
        ShowedFinalText.text = FeedBack.FinalScore.ToString();
        ShowedLevelText.text = FeedBack.LevelText.ToString();
    }

    void Awake()
    {
        FeedBack.instance = this;
        
    }


    // Update is called once per frame
    void Update()
    {

    }
}
