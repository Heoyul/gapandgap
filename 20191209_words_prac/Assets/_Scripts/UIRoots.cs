﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRoots : MonoBehaviour
{
    [SerializeField]
    public Text scoreUI;
    // Start is called before the first frame update
    public void OnGameStarted()
    {
        scoreUI.text = string.Format("{0}", 10);
    }
    public void OnScoreChanged(int score) 
    { 
        scoreUI.text = string.Format("{0}", score); 
    }

}
