﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class b_exit : MonoBehaviour
{
    public GameObject quitPanel;
    public Button quitBtn, quitYes, quitNo;
    Boolean quit = false;
    // Start is called before the first frame update
    public void OnClickExit() {
        quit = true;
        Debug.Log("quitBtn click");
        quitPanel.gameObject.SetActive(quit);
        quitYes.onClick.AddListener(() => Application.Quit());
        quitNo.onClick.AddListener(() => quitPanel.gameObject.SetActive(false));
        quit = false;
    }
  
}
