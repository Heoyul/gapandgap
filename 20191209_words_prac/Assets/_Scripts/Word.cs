﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Word
{
    public int level;
    public string word;
    public string meaning;
}
