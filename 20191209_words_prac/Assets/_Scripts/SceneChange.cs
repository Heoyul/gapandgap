﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    [SerializeField]
    public void changeMenuScene()
    {
        SceneManager.LoadScene(1);
    }
    public void changeLesson1Scene(int crl)
    {

        SceneManager.LoadScene(2);
        GameManager.currentLevel = crl;
    }
    public void ChangeFeedbackScene()
    {
        SceneManager.LoadScene(3);
        Debug.Log("씬 바뀜");
        FeedBack.instance.Final();
    }
    public void ChangeStartScene()
    {

        SceneManager.LoadScene(1);
    }
    public void ChangeHelp1Scene()
    {

        SceneManager.LoadScene(4);
    }
    public void ChangeHelp2Scene()
    {

        SceneManager.LoadScene(5);
    }
  
}
