﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.IO;



public class GameManager : MonoBehaviour
{
    [SerializeField] 
    UIRoots uIRoots;
    ScoreManager scoreManager;


    public string word, answer; // 입력받은 글자 
    SceneChange sceneChange;

    char[] spells;   // 쪼갠 글자
    List<char> shuffledSpellList;
    List<char> spellList;
    public List<char> clickedSpellList;
    public List<Button> clickedButtonList;
    public List<Button> letterButtons;
    public Button letterButton, clickedButton, retryButton;
    public GameObject shuffledPanel, clickedPanel, quitPanel;
    public Button rightBtn, wrongBtn, quitBtn, quitYes, quitNo;
    public Text meaning, showAnswer, scoreText, levelTxt;
    int wordIndex = 0;
    public TextAsset[] jsonFiles;
    public Action rightAnswer, gameStart, gameEnd;
    public static int currentLevel = 0;
     

    void Start()
    {
        gameStart?.Invoke();
        clickedButtonList = new List<Button>();
        clickedSpellList = new List<char>();
        uIRoots = gameObject.AddComponent<UIRoots>();
        uIRoots.scoreUI = this.scoreText;
        scoreManager = gameObject.AddComponent<ScoreManager>();
        sceneChange = gameObject.AddComponent<SceneChange>();

        BindEvents();
        AddListeners();
        newGame(currentLevel);
    }
    void AddListeners()
    {
        retryButton.onClick.AddListener(() => retryOnClick());
        rightBtn.onClick.AddListener(() => answerBtnOnClick());
        wrongBtn.onClick.AddListener(() => answerBtnOnClick());
    }

    void newGame(int currentLevel)
    {
       
        Debug.Log(wordIndex);
        answer = "";
        Words wordsInJson = JsonUtility.FromJson<Words>(jsonFiles[currentLevel].text);
        word = wordsInJson.words[wordIndex].word;
        levelTxt.text = wordsInJson.words[wordIndex].level.ToString();
        meaning.text = wordsInJson.words[wordIndex].meaning.ToString();
        showAnswer.text = "정답: " + wordsInJson.words[wordIndex].word.ToString();
        spells = word.ToCharArray();    //word를 스펠로 자르기
        spellList = arrayToList(spells);  //letter를 리스트에 넣음 (랜덤값돌려서 하나씩 삭제 할 부분)
        shuffledSpellList = listShuffling(spellList);
        letterButtons = makeLetterButtons(shuffledSpellList);
    }

    /*letter를 리스트에 넣음 (랜덤값돌려서 하나씩 삭제 할 부분) */
    List<char> arrayToList(char[] spellArray)
    {
        List<char> convertedList = new List<char>();
        for (int i = 0; i < spellArray.Length; i++)
        {
            convertedList.Add(spellArray[i]);
        }
        return convertedList;
    }


    /*랜덤값 돌려 shuffle리스트에 집어 넣고, 하나씩 리스트에서 삭제 함(중복예방)*/
    List<char> listShuffling(List<char> targetList)
    {
        List<char> shuffledList = new List<char>();

        while (targetList.Count > 0)
        {
            int r = UnityEngine.Random.Range(0, targetList.Count - 1);
            shuffledList.Add(targetList[r]);
            targetList.RemoveAt(r);
        }
        return shuffledList;

    }
    List<Button> makeLetterButtons(List<char> shffled)
    {
        letterButtons = new List<Button>();
        
        /*여러개의 letterButton을 생성함.*/
        for (int i = 0; i < shffled.Count; i++)
        {
            Button btn = Instantiate(letterButton);
            letterButtons.Add(btn);
            btn.transform.SetParent(shuffledPanel.transform);
            btn.GetComponentInChildren<Text>().text = shffled[i].ToString();
            char letter = shffled[i];
            btn.onClick.AddListener(() => OnClick(letter));
        }
        return letterButtons;
    }

    void BindEvents()
    {
        this.rightAnswer += scoreManager.AddScore;
        this.gameStart += uIRoots.OnGameStarted;
        scoreManager.ScoreChanged += uIRoots.OnScoreChanged;
        gameEnd += sceneChange.ChangeFeedbackScene;

    } 
    void UnBindEvents()
    {
        this.rightAnswer -= scoreManager.AddScore;
        this.rightAnswer -= uIRoots.OnGameStarted;
        scoreManager.ScoreChanged -= uIRoots.OnScoreChanged;
        gameEnd -= sceneChange.ChangeFeedbackScene;


    }

    void checkAnswer()
    {
        if (answer == word)
        {

            rightAnswer?.Invoke();
            Debug.Log("축하합니다");
            Debug.Log(word);

            rightBtn.gameObject.SetActive(true);

            destroyAllChildren(clickedPanel.transform);
            destroyAllChildren(shuffledPanel.transform);
          
            
        }
        else
        {
            wrongBtn.gameObject.SetActive(true);

            destroyAllChildren(clickedPanel.transform);
            destroyAllChildren(shuffledPanel.transform);

            
            Debug.Log("틀렸습니다.");
            Debug.Log(word);
        }
    }

    private void destroyAllChildren(Transform parent)
    {
        foreach (Transform child in parent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void answerBtnOnClick()
    {
        rightBtn.gameObject.SetActive(false);
        wrongBtn.gameObject.SetActive(false);
        
        if (wordIndex < JsonUtility.FromJson<Words>(jsonFiles[currentLevel].text).words.Length-1)
        {
            wordIndex += 1;
            newGame(currentLevel);
        }
        else
        {
            Debug.Log("더이상 문제가 없습니다.");
            gameEnd?.Invoke();
        }
       

    }
  

    void OnClick(char letter)
    {
        Button clickbtn = Instantiate(clickedButton);
        clickedButtonList.Add(clickbtn);
        clickedSpellList.Add(letter);
        clickbtn.GetComponentInChildren<Text>().text = letter.ToString();
        clickbtn.transform.SetParent(clickedPanel.transform);

        for (int i = clickedSpellList.Count - 1; i < clickedSpellList.Count; i++)
        {
            char c = clickedSpellList[i];
            answer += c;
            Debug.Log(answer);
        }
        if (clickedPanel.transform.childCount == shuffledPanel.transform.childCount)
        { 
            checkAnswer();
            Debug.Log("정답확인할것");
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void retryOnClick()
    {
        destroyAllChildren(clickedPanel.transform);
        destroyAllChildren(shuffledPanel.transform);
        newGame(currentLevel);
    }

}

