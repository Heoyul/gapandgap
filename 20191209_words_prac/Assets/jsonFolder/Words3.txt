{
  "words":
  [
    {
      "level": 3,
      "word": "April",
	  "meaning" : "4월"
    },
    {
      "level": 3,
       "word": "August",
	   "meaning" : "8월"
    },
    {
       "level": 3,
       "word": "birthday",
	   "meaning": "생일"
    },
    {
       "level": 3,
       "word": "concert",
	   "meaning": "콘서트"
    },
    {
       "level": 3,
       "word": "contest",
	   "meaning": "대회"
    },
    {
       "level": 3,
       "word": "December",
	   "meaning": "12월"
    },
    {
       "level": 3,
       "word": "drone",
	   "meaning": "드론"
    },
    {
       "level": 2,
       "word": "nose",
	   "meaning": "코"
    },
    {
       "level": 3,
       "word": "face",
	   "meaning": "얼굴"
    },
    {
       "level": 3,
       "word": "February",
	   "meaning": "2월"
    },
    {
       "level": 3,
       "word": "January",
	   "meaning": "1월"
    },
    {
       "level": 3,
       "word": "July",
	   "meaning": "7월"
    },
    {
       "level": 6,
       "word": "June",
	   "meaning": "6월"
    },
    {
       "level": 3,
       "word": "March",
	   "meaning": "3월"
    },
    {
       "level": 3,
       "word": "May",
	   "meaning": "5월"
    },
    {
       "level": 3,
       "word": "November",
	   "meaning": "11월"
    },
    {
       "level": 3,
       "word": "October",
	   "meaning": "10월"
    },
    {
       "level": 3,
       "word": "or",
	   "meaning": "또는"
    },
    {
       "level": 3,
       "word": "sell",
	   "meaning": "팔다"
    },
    {
       "level": 3,
       "word": "September",
	   "meaning": "9월"
    },
    {
       "level": 3,
       "word": "ukulele",
	   "meaning": "우쿠렐레"
    },
    {
       "level": 3,
       "word": "when",
	   "meaning": "언제"
    }
  ]
}