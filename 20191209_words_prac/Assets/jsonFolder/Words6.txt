{
  "words":
  [
    {
      "level": 6,
      "word": "airport",
	  "meaning" : "공항"
    },
    {
      "level": 6,
       "word": "beutiful",
	   "meaning" : "아름다운"
    },
    {
       "level": 6,
       "word": "brown",
	   "meaning": "갈색의"
    },
    {
       "level": 6,
       "word": "curly",
	   "meaning": "곱슬곱슬한"
    },
    {
       "level": 6,
       "word": "eye",
	   "meaning": "눈"
    },
    {
       "level": 6,
       "word": "over",
	   "meaning": "너머"
    },
    {
       "level": 6,
       "word": "pink",
	   "meaning": "분홍색"
    },
    {
       "level": 6,
       "word": "short",
	   "meaning": "짧"
    },
    {
       "level": 6,
       "word": "student",
	   "meaning": "학생"
    },
    {
       "level": 6,
       "word": "worry",
	   "meaning": "걱정하다"
    }
  ]
}